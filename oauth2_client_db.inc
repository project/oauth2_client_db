<?php

namespace OAuth2;

/**
 * @file
 * Class OAuth2\DatabaseClient.
 */

/**
 * The class OAuth2\DatabaseClient is used to store token data in database.
 */
class DatabaseClient extends Client {

  /**
   * The base table for storing token data.
   *
   * @var string
   */
  protected static $tableName = 'oauth2_client';

  /**
   * {@inheritdoc}
   */
  public static function loadToken($key) {
    $tokens = &drupal_static(__CLASS__, []);

    if (isset($tokens[$key]['access_token'])) {
      return $tokens[$key];
    }

    $tokens[$key] = self::emptyToken();

    $token = db_select(self::$tableName, 'c')
      ->fields('c', ['token'])
      ->condition('c.client_id', $key)
      ->execute()
      ->fetchField();

    if (!empty($token)) {
      $tokens[$key] = unserialize($token);
    }

    return $tokens[$key];
  }

  /**
   * {@inheritdoc}
   */
  public static function storeToken($key, array $token) {
    $tokens = &drupal_static(__CLASS__, []);

    db_merge(self::$tableName)
      ->key(['client_id' => $key])
      ->fields(['token' => serialize($token)])
      ->execute();

    // Update the token data in static as well.
    $tokens[$key] = $token;
  }

  /**
   * {@inheritdoc}
   */
  protected static function discardToken($key) {
    $tokens = &drupal_static(__CLASS__, []);

    db_delete(self::$tableName)
      ->condition('client_id', $key)
      ->execute();

    // Clean up static.
    if (isset($tokens[$key])) {
      unset($tokens[$key]);
    }
  }

}
