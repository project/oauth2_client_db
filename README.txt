OAuth2 Client Database
----------------------
OAuth2 Client Database integrates OAuth2 Client into Drupal and allows
you to store the token data in the database storage instead of session storage.

Installation:
-------------
Install as any other module:
https://www.drupal.org/node/895232

Dependencies:
-------------
- OAuth2 Client (>= 7.x-2.0) (https://www.drupal.org/project/oauth2_client)

Usages:
---------
<?php
$oauth2_config = [
  'token_endpoint' => $server_url . '/oauth2/token',
  'auth_flow' => 'user-password',
  'client_id' => 'YOUR_CLIENT_ID',
  'client_secret' => 'YOUR_CLIENT_SECRET',
  'username' => $username,
  'password' => $password,
];
try {
  // Determines the ID by which access token will be fetched from db.
  $client_id = "oauth2_client:{$user->uid}";

  // Init client and fetch access token.
  $oauth2_client = new OAuth2\DatabaseClient($oauth2_config, $client_id);
  $access_token = $oauth2_client->getAccessToken();
}
catch (Exception $e) {
  drupal_set_message($e->getMessage(), 'error');
}

Troubleshooting:
----------------
If you have an issue with OAuth2 Client - please check
https://www.drupal.org/project/issues/oauth2_client

If nothing helps - welcome to the issue queue at
https://www.drupal.org/project/issues/oauth2_client_db.
